/*~PF-*/var Sites/*~PF+*/ = (function () {
	var views={};
	var sitios={};
	var x = {
		url: "view/",
		funcion: "Sites"
	};

	var rutas = {};

	var preArchSite = "site_";
	return {
		titulo: function (t) {
			document.title = t;
		},
		init: function (params) {
			var base = $("base").attr("href");
			if (base == "") {
				console.error("Sites: Se requiere una etiqueta <base href='ruta_base/'>");
				return false;
			}
			HashHistoryIniciar(base, "Sites.url", "ir");
			if (base.slice(-1) != "/") {
				console.error("Sites: La etiqueta base debe terminar en \"/\"");
				return false;
			};
			var url = location.href.split(base)[1];
			EXE.ADD({
				nombre: preArchSite + "index",
				url: 'controller/sites/' + preArchSite + 'index.js',
				funcion: preArchSite + "index"
			});
			EXE.F(preArchSite + "index", {
				url: url
			}, "init");
			/*
			Sites.GET({
				url: sitio + ".php",
				callback: function () {
					EXE.F(preArchSite + sitio, { url: url }, "init");
				}
			});
			*/
		},
		GET: function (params) {
			
			if(sitios[params.url]){
				$("#__site__").html(sitios[params.url]);
				params.callback();
				return false;
			};
			//creamos otra variable para NO usar la global
			var z = {
				url: x.url + params.url,
				funcion: x.funcion
			};
			z.type = "GET";
			z.data = "";
			z.selfCallback = Sites.GET;
			z.selfCallbackVars = params;
			z.json = false;
			z.callback = function (html) {
				sitios[params.url]=html;
				$("#__site__").html(html);
				params.callback();
			};
			Ajax_(z);
		},
		ir: function (url) {
			console.info("Sites: dirigir a \"" + url + "\" ");
			ir(url);
		},
		url: function (url) {
			Sites.init();
		},
		SASS: function (CSS_array) {
			CSS_array.map(function (c) {
				$("head").append('<link class="sass" rel="stylesheet" type="text/css" data-href="' + c + '" href="">');
			});
			sass.init();
		},
		html:function(J) {
			$("#__site__").html(J);
		},
		getView: function (params) {
			if(params.url=="" ||  params.url ==undefined || params.url ==null){
				console.error("Sites: getView: La url solicitada no puede ser vacía");
				return false;
			}
			if(views[params.url]){
				params.back(views[params.url]);
				params.cargada=true;
				return false;
			};
			//creamos otra variable para NO usar la global
			var z = {
				url: params.url,
				funcion: x.funcion
			};
			z.type = "GET";
			z.data = "";
			z.json = false;
			z.callback = function (J) {
				views[params.url]=J;
				params.back(J);
				params.cargada=true;
			};
			Ajax_(z);
			params.cargada=0;
			/*~PF-*/var f/*~PF+*/=function(params) {
				if(typeof params.cargada == "number" && params.cargada>20){
					MJ_simple("cargando sitio","No ha sido posible cargar el sitio");
					params.cargada=false;
				}else if(typeof params.cargada == "number"){
					params.cargada++;
					setTimeout(function() {
						f(params);
					}, 100);
				}
			};
			f(params);
		}
	}
})();
setTimeout(Sites.init, 60);