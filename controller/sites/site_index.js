/*~PF-*/var site_index/*~PF+*/ = (function () {
	var view={
		index:"VIEW.index"
	};
	/**
	 * función que permite ejecutar la función principal del index
	 * @param {json} params opcional pero en caso de existir se requiere un parámetro callback
	 */
	function cargar_index(params) {
		var vista={
			url:view.index,
			back:function(J) {
				Sites.html(J);
				$("#fun_notas").attr("onclick","Sites.ir('"+Sites.rutas.notas.get+"');");
				if(params)
					params.callback();
			}
		};
		Sites.getView(vista);
	};

	return {
		init:function(params) {
			switch (params.url) {
				case Sites.rutas.index.get:
					Sites.titulo("Bienvenido");
					cargar_index();
					break;
				case Sites.rutas.notas.get:
					EXE.ADD({	nombre:"site_notas",	url:'controller/sites/site_notas.js',	funcion:"site_notas"});
					cargar_index({
						callback:function() {
							EXE.F("site_notas",{},"init");
						}
					});
					break;
			}
		}
	}
})();