var reTry=Array();
var reTryInternet=null;
var reTryInternetCOLORtext="FFFFFF";
var reTryInternetCOLORbackg="180,0,0";
setTimeout("verifInternet();",5000);
function FReTry(nombre,ajaxStatus,ajaxStatusTxt){
	/*
	FReTry(str nombre,ajax.status ajaxStatus);
	se encarga de almacenar los intentos y se muestra el mensaje correspondiente
	return [true,false,""]
	*/
	
	if(ajaxStatusTxt==undefined)ajaxStatusTxt="TextStatus no definido";
	if(!reTry[nombre])reTry[nombre]=0;
	switch(ajaxStatus){
		case 200:
			reTry[nombre]=0;
			return true;
			break;
		case "0":
		case 0:
			reTry[nombre]+=1;
			if(reTryInternet==true){
				if(reTry[nombre]>8 || ajaxStatusTxt=="Internal Server Error"){
					alert('Ha ocurrido un problema, reporte el siguiente error: "'+nombre+'-500:'+ajaxStatusTxt+'"');
					retRespuesta=""
				}else retRespuesta=false;
			}else{
				reTry[nombre]=0;
				if(!confirm('Compruebe su conexion a internet y "Aceptar" para volver a intentarlo.'))retRespuesta="";else retRespuesta=false;
			};
			return retRespuesta;
			break;
		default:
			reTry[nombre]+=1;
			retRespuesta=false;
			if(reTry[nombre]>10)if(!confirm('Ha ocurrido un problema, desea reintentar? si vuelve a fallar reporte el siguiente error: "'+nombre+'-'+ajaxStatus+':'+ajaxStatusTxt+'"'))retRespuesta="";else retRespuesta=false;
			return retRespuesta;
			break;
	}
}
function verifInternet(){
	/*
	verifInternet();
	se encarga de verificar la conexión de internet muestra y oculta el mensaje de falta de internet
	*/
	var i = new Image();
	i.onload=function(){
		if(reTryInternet==false)$("#reTryInternetmensaje").remove();
		reTryInternet=true;
		setTimeout("verifInternet()",3000);
	};
	i.onerror=function(){
		if(reTryInternet==true||reTryInternet==null)$("body").append('<div align="center" style="font-family:arial;z-index:1000000000;color:#'+reTryInternetCOLORtext+'; font-size:0.8em; width:21em;max-width:100%;background:rgb('+reTryInternetCOLORbackg+');background:rgba('+reTryInternetCOLORbackg+',0.7); position:fixed; left:0px; top:0px;" id="reTryInternetmensaje">Sin conexi&oacute;n a internet</div>');
		reTryInternet=false;
		setTimeout("verifInternet()",3000);
	};
	i.src = 'recursos/pixel.png?d=' + escape(Date());
}