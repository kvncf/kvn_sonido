<?
require_once "c.a.FacilJSON.php";
require_once "c.EXCEPCION.php";
/**
 * clase abstracta REST que estandariza la recepción de peticiones REST con PUT,GET,POST y DELETE
*/
abstract class REST extends absFacilJSON{

	protected $DATA=array();
	private $errorNoPermitido="Verbo de petición NO PERMITIDO";

	/**
	 * constructor salvo que el parametro se especifique false, verifica qué verbo se pide, combina variables en DATA y ejecuta 
	 * ejecutar=true: por defecto auto ejecuta pero si se envía false no lo hace
	*/
	function __construct($ejecutar) {
		parent::__construct();
		try {
			if($ejecutar){
				$this->getVars();//leer todo $DATA= $_PUT,$_POST y $_GET
				//identificamos el verbo
				$this->beforeExecute();
				$verbo=(isset($_POST["__method__"]))?$_POST["__method__"]:$_SERVER['REQUEST_METHOD'];
				switch($verbo){
					case "GET": $this->get();break;
					case "POST": $this->post();break;
					case "PUT": $this->put();break;
					case "DELETE": $this->delete();break;
					default: die("Verbo no definido: ".$verbo);break;
				}
			}
		} catch (Exception $e) {
			EXCEPCION::show($e);
		}
	}

	/**
	 * limpia los arreglos
	*/
	private function cleanInputs($data){
		$clean_input = array();
		if(is_array($data)){
			foreach($data as $k => $v){
				$clean_input[$k] = $this->cleanInputs($v);
			}
		}else{
			if(get_magic_quotes_gpc()){
				$data = trim(stripslashes($data));
			}
			$data = strip_tags($data);
			$clean_input = trim($data);
		}
		return $clean_input;
	}

	/**
	 * Recibo variables
	*/
	private function getVars(){
		$post=array();
		$get=array();//incluye DELETE
		$put=array();
		$post = $this->cleanInputs($_POST);
		$get = $this->cleanInputs($_GET);
		if($_SERVER['REQUEST_METHOD']=="PUT"){
			parse_str(file_get_contents("php://input"),$put);
			$put = $this->cleanInputs($put);
		}
		$this->DATA=array_merge(array_merge($post,$get),$put);
	}

	
	/**
	 * Se ejecuta antes de el GET, POST, DELETE y PUT
	*/
	protected function beforeExecute(){
	}

	/**
	 * Método que ejecuta la petición GET
	*/
	protected function get(){
		die($this->errorNoPermitido." -> GET");
	}
	/**
	 * Método que ejecuta la petición DELETE
	*/
	protected function delete(){
		die($this->errorNoPermitido." -> DELETE");
	}
	/**
	 * Método que ejecuta la petición POST
	*/
	protected function post(){
		die($this->errorNoPermitido." -> POST");
	}
	/**
	 * Método que ejecuta la petición PUT
	*/
	protected function put(){
		die($this->errorNoPermitido." -> PUT");
	}

	/**
	 * revisa que las variables sean definidas
	 */
	protected final function revisarVariables($vars){
		foreach ($vars as $k)
			if(!isset($this->DATA[$k]))
				throw new Exception("No se ha enviado la variable requerida: ".$k, 1);
	}
}
?>