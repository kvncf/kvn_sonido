/*~PF-*/var notas/*~PF+*/ = (function () {
	var notas_=Array('C','D','E','F','G','A','B');
	var nota=null;

	return {
		init: function (params) {
			$("#iniciar").hide();
			$(".nota").show();
			var rand=Math.floor(Math.random() * notas_.length);
			notas.setNota(notas_[rand]);
			if(!params.test)
				document.getElementById('audio'+nota).play();
		},
		/**
		 * valida la nota ingresada si es correcta o incorrecta
		 */
		presiona:function(params) {
			if(params===undefined || params.nota===undefined)
				/*~PF-*/throw "se requiere el parámetro nota";/*~PF+*/
			if(nota==params.nota){
				alert("correcto");
				notas.init(params);
				return true;
			}else{
				alert("mal :(");
				if(!params.test)
					document.getElementById('audio'+nota).play();
				return false;
			}
		},
		setNota:function(_nota) {
			nota=_nota;
		}
	}
})();