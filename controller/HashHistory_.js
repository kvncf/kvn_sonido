var soportaHashHistory = null;
var HashHistoryURLbase = "";
var HashHistoryfunction = "";
var HashHistoryClassHref = "";
function HashHistoryIniciar(_HashHistoryURLbase, _HashHistoryfunction, _HashHistoryClassHref) {
	HashHistoryURLbase = _HashHistoryURLbase;
	HashHistoryfunction = _HashHistoryfunction;
	HashHistoryClassHref = _HashHistoryClassHref;
	// Para navegadores que soportan la función.
	if ( /*~saa~K~O~N~*/typeof window.history.pushState  /*~enn~K~O~N~*/ == 'function') {
		soportaHashHistory = true;
		pushstate();
	} else {
		soportaHashHistory = false;
		check();
		hashHashHistory();
	}
};
// Chequear si existe el hash.
function check() {
	var direccion = "" + window.location + "";
	var nombre = direccion.split("#");
	if (nombre.length > 1) {
		var url = nombre[1];
		location.href = HashHistoryURLbase + url;
	}
};
function pushstate() {
	if (HashHistoryClassHref != "") {
		var links = $("." + HashHistoryClassHref);
		// Evento al hacer click.
		if (links.length > 0) links.live('click', function (event) {
			var url = $(this).attr('href');
			// Cambio el historial del navegador.
			HashHistory(url);
			return false;
		});
	}
	// Función para determinar cuando cambia la url de la página.
	$(/*~saa~K~O~N~*/window/*~enn~K~O~N~*/).bind('popstate', function (event) {
		var state = event.originalEvent.state;
		if (state) 
			ejecutarFuncionHH(location.href,false);
	});
};
function hashHashHistory() {
	// Para i.e
	// Función para determinar cuando cambia el hash de la página.
	$(/*~saa~K~O~N~*/window/*~enn~K~O~N~*/).bind("hashchange", function () {
		var hash = "" + window.location.hash + "";
		hash = hash.replace("#!", "");
		if (hash && hash != "")
			ejecutarFuncionHH(hash,true);
			if (HashHistoryfunction == "") alert(hash); else eval(HashHistoryfunction + "('" + hash.replace(HashHistoryURLbase, "") + "');");
	});
};

function ir(url) {
	HashHistory(url);
};

function HashHistory(url) {
	if (soportaHashHistory) {
		url = HashHistoryURLbase + url;
		setTimeout(function () {
			history.pushState({ path: url }, document.title, url);
			ejecutarFuncionHH(url,false);
		}, 200);
	} else window.location.hash = url;
};

function ejecutarFuncionHH(url,sinBase) {
	if (!sinBase)
		url=url.split(HashHistoryURLbase)[1];
	if (HashHistoryfunction == "") alert(hash);
	else eval(HashHistoryfunction + "('" + url + "');");
};