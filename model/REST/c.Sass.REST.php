<?
require_once "../kvn/c.a.REST.php";
class Sass extends REST{

	private $raiz="../../";
	private $rutaAbs;

	/**
	revisa si ya se procesaron los archivos
	 */
	function get(){
		//buscamos la ruta abs
		$dir=explode("/",$_SERVER["SCRIPT_FILENAME"]);
		$this->rutaAbs=str_replace($dir[count($dir)-1],"",$_SERVER["SCRIPT_FILENAME"]);

		$this->DATA["mobile"]=$this->DATA["mobile"]=="true"?"1":"0";
		$allContent="";

		//buscamos los archivos
		$archivosSalida=array();
		$archivos=json_decode($this->DATA["archivos"],true);
		foreach($archivos as $id => $ruta) {
			$Original=$this->raiz.$ruta.".scss";
			$rutaArchivoNuevo=$ruta."_".($this->DATA["mobile"]=="1"?"mobile":$this->DATA["w"])."__.css";
			$Final=$this->raiz.$rutaArchivoNuevo;
			if($this->revisarSass($Original,$Final)){
				$archivosSalida[$id]=$rutaArchivoNuevo;
				$f=fopen($Final, "r");
				$allContent.=fread($f,filesize($Final)+1);
			}

		}
		$this->json("all",$allContent);
		$this->json("archivos",$archivosSalida);
		$this->send();
	}

	private function revisarSass($Original,$Final){
		#primero revisamos que exista el archivo scss
		if(!file_exists($Original)){
			$this->falla("archivo no existe:".$Original);
			return false;
		}
		if(!file_exists($Final) || filemtime($Original)>filemtime($Final)){
			$_ruta="cd ".$this->rutaAbs;
			$_reemplazo1="sed -i -re 's/([a-z]{2}\.)?W:1;/W:".$this->DATA["w"].";/g' ".$Original;
			$_reemplazo11="sed -i -re 's/([a-z]{2}\.)?mobile:1;/mobile:".$this->DATA["mobile"].";/g' ".$Original;
			$_sass="sass ".$Original." ".$Final." --style compressed --sourcemap=none";
			$_reemplazo2="sed -i -re 's/([a-z]{2}\.)?W:".$this->DATA["w"].";/W:1;/g' ".$Original;
			$_reemplazo22="sed -i -re 's/([a-z]{2}\.)?mobile:".$this->DATA["mobile"].";/mobile:1;/g' ".$Original;
			echo shell_exec($_ruta." ; ".$_reemplazo1." ; ".$_reemplazo11." ; ".$_sass." ; ".$_reemplazo2." ; ".$_reemplazo22);
		}
		return true;
	}
}
new Sass(true);
?>