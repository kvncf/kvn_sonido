/*~PF-*/var EXE/*~PF+*/ = (function () {
	
	var js={};

	var SETfun=function(nombre,f) {
		js[nombre].fun=f;
	};

	return {
		ADD:function(params) {
			if(params==undefined){
				console.error("EXE: se requiere como parámetro un JSON");
				return false;
			}
			if(params.nombre==undefined || params.nombre==""){
				console.error("EXE: se requiere como parámetro un JSON.nombre");
				return false;
			}
			if(params.funcion==undefined || params.funcion==""){
				console.error("EXE: se requiere como parámetro un JSON.funcion");
				return false;
			}
			if(params.url==undefined){
				console.error("EXE: se requiere como parámetro un JSON.url");
				return false;
			}
			if(js[params.nombre]!=undefined){
				console.info("EXE: omitido -> \""+params.nombre+"\"");
				return false;
			}
			js[params.nombre]={
				url:params.url,
				fun:undefined,
				funcion:params.funcion
			};
			console.info("EXE: agregado \""+params.nombre+"\"");
		},
		SETfun:function(nombre,f) {
			js[nombre].fun=f;
		},
		F:function(nombre,p,f="") {
			if(js[nombre]){//ya agregado
				if(js[nombre].fun!=undefined)
					if(f==undefined)
						eval(js[nombre].funcion+"("+JSON.stringify(p, (k,v) => typeof v === "function" ? "" + v : v)+");");
					else 
						eval(js[nombre].funcion+"."+f+"("+JSON.stringify(p, (k,v) => typeof v === "function" ? "" + v : v)+");");
				else{
					$.ajax({
						url:js[nombre].url,
						success: function(result){
							$("head").append("<script EXE-nombre='"+nombre+"'>"+result+"<\/script>");
							eval("EXE.SETfun('"+nombre+"',"+nombre+");");
							EXE.F(nombre,p,f);
						}
					})
				};
			}else 
				console.error("EXE: \""+nombre+"\" NO agregado aún usar EXE.ADD({nombre:\""+nombre+"\",url:'',funcion:''}); para añadir");
		},
		X:function(nombre,p,f=undefined) {
			if(js[nombre]){//ya agregado
				if(js[nombre].fun!=undefined)
					if(f==undefined)
						return js[nombre].fun(p);
					else 
						f(p);
				else{
					$.ajax({
						url:js[nombre].url,
						success: function(result){
							$("head").append("<script EXE-nombre='"+nombre+"'>"+result+"<\/script>");
							eval("EXE.SETfun('"+nombre+"',"+js[nombre].funcion+");");
							EXE.X(nombre,p,f);
						}
					})
				}
			}else 
				console.error("EXE: \""+nombre+"\" NO agregado aún usar EXE.ADD({nombre:\""+nombre+"\",url:'',funcion:''}); para añadir");
		}
	};
})();