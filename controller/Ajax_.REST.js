/*~PF-*/var Ajax_/*~PF+*/ = function (settings) {
	/*
	url
	type
	data
	funcion
	selfCallback
	selfCallbackVars
	json
	callback
	*/
	$.ajax({
		url: settings.url,
		type: (settings.type == "PUT" || settings.type == "DELETE") ? "POST" : settings.type,
		data: (settings.type == "GET"?"":"__method__=" + settings.type + "&")+ settings.data
	}).fail(function (jqXHR, textStatus) {
		if (FReTry(settings.funcion + settings.type, textStatus) === false) setTimeout(function () { settings.selfCallback(settings.selfCallbackVars); }, 1000);
	}).done(function (msg) {
		var J = msg;
		if (settings.json) {
			J = Array();
			try {
				J = eval("(" + msg + ")");
			} catch (/*~PF-*/e/*~PF+*/) {
				J = Array("res", "mj");
				J["mj"] = msg;
				J["res"] = false;
				J["error"] = true;
			}
			if (J["error"]) {
				J["mj"] +="<br><br><div style='text-align:left;'><a href='javascript:$(\"#trace_excepcion_Ajax\").show();'>Informar error</a></div><textarea id='trace_excepcion_Ajax' style='display:none;width:100%;height:400px;' readonly>Informa el error a: sistemas@konrradf.com\n\n";
				if(J["trace"])J["trace"].map(function(e) {
					J["mj"] +=e+"\n";
				});
				J["mj"] +="</textarea>";
				J["res"] = false;
			}
		}
		settings.callback(J);
	});
};