/** 
 * revisa si es versión mobile o pc
*/
/*~PF-*/var Mobile/*~PF+*/ = (function () {
	
	var is=null;
	/**
	 * revisa si es mobile o no
	 */
	var check=function() {
		$("head").append('<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1" />');
		$("body").append('<input type="hidden" id="dispositivo" value="pc" /><div id="medidaParaMovile" style="height:0px;width: 8cm;"></div>');
		is=!(window.innerWidth>2*$("#medidaParaMovile").width());
		$("#dispositivo").val(is?"m":"pc");
		$("#medidaParaMovile").remove();
	};

	return {
		/**
		 * función que se ejecuta al iniciar el sitio
		 */
		init:function () {
			setTimeout(check,100);
		},
		/**
		 * revisa si el usuario ya inició sesión
		 */
		is: function () {
			return is;
		}
	};
})();
Mobile.init();