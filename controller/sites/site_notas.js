/*~PF-*/var site_notas/*~PF+*/ = (function () {
	var view={
		notas:"VIEW.notas"
	};
	return {
		init:function() {
			EXE.ADD({	nombre:"notas",	url:'controller/own/notas.js',	funcion:"notas"});
			Sites.titulo("Notas");
			var vista={
				url:view.notas,
				back:function(J) {
					$("#contenido").html(J);
					$("#nota_C").attr("onclick","EXE.F('notas',{nota:'C'},'presiona');");
					$("#nota_D").attr("onclick","EXE.F('notas',{nota:'D'},'presiona');");
					$("#nota_E").attr("onclick","EXE.F('notas',{nota:'E'},'presiona');");
					$("#nota_F").attr("onclick","EXE.F('notas',{nota:'F'},'presiona');");
					$("#nota_G").attr("onclick","EXE.F('notas',{nota:'G'},'presiona');");
					$("#nota_A").attr("onclick","EXE.F('notas',{nota:'A'},'presiona');");
					$("#nota_B").attr("onclick","EXE.F('notas',{nota:'B'},'presiona');");
					$("#audioC").attr("src","recursos/notas/C.mp3");
					$("#audioD").attr("src","recursos/notas/D.mp3");
					$("#audioE").attr("src","recursos/notas/E.mp3");
					$("#audioF").attr("src","recursos/notas/F.mp3");
					$("#audioG").attr("src","recursos/notas/G.mp3");
					$("#audioA").attr("src","recursos/notas/A.mp3");
					$("#audioB").attr("src","recursos/notas/B.mp3");
					$("#iniciar").attr("onclick","EXE.F('notas',{},'init');");
				}
			};
			Sites.getView(vista);
		}
	}
})();