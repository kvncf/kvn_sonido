describe('notas', function () {
	it('presiona: si no se envia el parametro nota debe lanzar una excepción', function () {
		try {
			notas.presiona();
			fail("no lanza excepcion");
		} catch (error) {
			expect(true).toBe(true);	
		}
	});
	it('presiona: la nota incorrecta debe dar false', function () {
		notas.setNota("A");
		expect(notas.presiona({	nota:"C",	test:true})).toBe(false);
	});
	it('presiona: la nota correcta debe dar correcto', function () {
		notas.setNota("A");
		expect(notas.presiona({	nota:"A",	test:true})).toBe(true);
	});
	it('presiona: la nota correcta debe dar un intento más y un puntaje más', function () {
		notas.reset();
		notas.setNota("A");
		notas.presiona({	nota:"A",	test:true});
		expect(notas.getPuntaje()).toBe(1);
		expect(notas.getIntentos()).toBe(1);
	});
	it('presiona: la nota incorrecta debe dar un intento más y un puntaje menos', function () {
		notas.reset();
		notas.setNota("A");
		notas.presiona({	nota:"A",	test:true});
		expect(notas.getPuntaje()).toBe(1);
		expect(notas.getIntentos()).toBe(-1);
	});
	it('presiona: al llegar al puntaje max se debe terminar', function () {
		notas.reset();
		for (let i = 0; i < notas.getPuntajeMax(); i++) {
			notas.setNota("A");
			notas.presiona({	nota:"A",	test:true});	
		}
		expect(notas.terminado()).toBe(true);
	});
	it('debe permitir reiniciar el puntaje e intentos', function () {
		notas.reset();
		expect(notas.getPuntaje()).toBe(0);
		expect(notas.getIntentos()).toBe(0);
	});
});