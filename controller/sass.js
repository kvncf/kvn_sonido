/** 
 * sass
*/
var sass = (function () {
	var x = {
		url: "REST.Sass",
		funcion: "sass"
	};
	var id=0;
	var pendientes=[];

	/*~PF-*/var yaAgregado/*~PF+*/=function(archivo) {
		var ya=false;
		$(".sass-procesado").each(function () {
			var a=$(this).attr("data-archivos");
			var as=a.split(",");
			i=0;
			while (as[i]) {
				if(as[i]==archivo){
					ya=true;
					break;
				}
				i++;
			}
		});
		return ya;
	};

	return {
		/**
		 * función que se ejecuta al iniciar el sitio
		 */
		init:function () {
			id=0;
			var archivos=Array();
			pendientes=Array();
			$(".sass").each(function () {
				$(this).attr("class","").attr("id","sass"+id);
				var tmp=$(this).attr("data-href");
				if(yaAgregado(tmp))$(this).remove();
				else{
					archivos[id]=tmp;
					pendientes[id]=tmp;
					id++;
				}
			});
			if(archivos.length>0)
				sass.GET(archivos);
		},
		/**
		 * obtiene los codigos css procesados y comprimidos y elimina las etiquedas import de css que NO tienen problemas
		 */
		GET: function (params) {
			//creamos otra variable para NO usar la global
			var z = {
				url: x.url,
				funcion: x.funcion
			};
			z.type = "GET";
			z.data = "archivos="+encodeURIComponent(JSON.stringify(params))+"&w="+screen.width+"&mobile="+Mobile.is();
			z.selfCallback = sass.GET;
			z.selfCallbackVars = params;
			z.json = true;
			z.callback = function (J) {
				if (!J["res"]) {
					MJ_simple("archivos css",J["mj"]);
				}else{
					J["archivos"].map(function(url,id) {
						$("#sass"+id).remove();
					});
					$("head").append("<style class='sass-procesado' data-archivos='"+pendientes.join(",")+"'>"+J["all"]+"</style>");
				}
			};
			Ajax_(z);
		}
	}
})();
setTimeout(sass.init,50);