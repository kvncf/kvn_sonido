<?

class EXCEPCION{

	public static function show($e){
		$r=array();
		$r["res"]=false;
		$r["error"]=true;
		$r["mj"]=$e->getMessage();
		$r["trace"]=array();
		$r["trace"][]="--> ".$e->getMessage();
		$n=0;
		foreach ($e->getTrace() as $a) {
			$r["trace"][]="(".$n.") ".$a["class"]."::".$a["function"];
			$n++;
		}
		echo json_encode($r);
	}

	public static function toString($e){
		echo 'Excepción: <b>'.$e->getMessage()."</b><br>";
		$n=0;
		foreach ($e->getTrace() as $a) {
			echo "(".$n.") ".$a["class"]."::".$a["function"]."<br>";
			$n++;
		}
	}
}
?>